/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.utils.commands;

import me.jantuck.utilities.reflection.ReflectionUtil;
import me.jantuck.utils.reflection.MinecraftReflectionProvider;
import org.bukkit.command.annotation.Command;
import org.bukkit.command.annotation.CommandExecutorV2;
import org.bukkit.command.annotation.CommandReceiver;
import org.bukkit.command.annotation.CommandUsable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 * Created by Jan on 27-01-2017.
 */
public class Commands extends CommandExecutorV2 {


    @Command(name = "getitemname", description = "Gets the name of the item in your hand", usage = "/getitemname", usableBy = CommandUsable.User, permission = "this.name",
            notEnoughPermissionMessage = "&cYou don't have the correct permissions huh.", aliases = "[fun,stupid,who,lol]")
    public void mySuperAwesomeCustomCommand(CommandReceiver commandReceiver) {
        Player player = (Player) commandReceiver.getCommandSender();
        player.sendMessage("ItemStack: " + getName(player.getInventory().getItemInMainHand()));
    }

    public String getName(ItemStack itemStack) {
        final String[] item = {itemStack.getType().name()};
        ReflectionUtil.newCall().getMethod(MinecraftReflectionProvider.CRAFT_ITEMSTACK, "asNMSCopy", ItemStack.class)
                .get().passIfValid(reflectionMethod -> {
            Object nmsItemStack = reflectionMethod.invokeIfValid(null, itemStack);
            item[0] = ReflectionUtil.newCall().getMethod(MinecraftReflectionProvider.NMS_ITEMSTACK, "getName").get().invokeIfValid(nmsItemStack);
        });
        return item[0];
    }
}
